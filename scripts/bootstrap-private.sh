#!/usr/bin/env bash

cd $HOME/utils/private

# copy ssh keys and ssh config

echo -e "\e[1;31m SSH COPY \e[0m"
declare -a sshKeyNames=(id_ed25519)
sshdir="${HOME}/.ssh/"
echo -e "\e[1;31m ${sshdir} \e[0m"

mkdir -p ${sshdir}
cp -rf ssh/* /$HOME/.ssh/
chmod 700 ${sshdir}
chmod 644 ${sshdir}config
chmod 600 ${sshdir}authorized_keys
chmod 600 ${sshdir}known_hosts
for sshKey in ${sshKeyNames[@]}; do 
    chmod 400 ${sshdir}${sshKey}
    chmod 644 ${sshdir}${sshKey}.pub
done

# add gpg keys
echo -e "\e[1;31m GPG COPY \e[0m"
cat key_password | gpg --import --batch --yes --passphrase-fd 0 gpg --import gpg/mypubkeys.asc
cat key_password | gpg --import --batch --yes --passphrase-fd 0 gpg/myseckeys.asc
cat key_password | gpg --import --batch --yes --passphrase-fd 0 gpg/mysecsubkeys.asc
gpg --import-ownertrust gpg/ownerstrust.txt
echo "026DF53D6B7C0B00732D555CF8E3EF51CF4F4F51:6:" | gpg --import-ownertrust

# copy kubeconfig files
mkdir -p $HOME/.kube
echo -e "\e[1;31m KUBE COPY \e[0m"
cp -rf kube/* $HOME/.kube/
# copy k3s config file 
cat /etc/rancher/k3s/k3s.yaml >> $HOME/.kube/config


# copy zsh history (that might contain sensitive information)
echo -e "\e[1;31m ZSH COPY \e[0m"
cp zsh/.zsh_history $HOME/

# copy aws config
echo -e "\e[1;31m AWS COPY \e[0m"
mkdir -p $HOME/.aws
cp -rf aws/* $HOME/.aws/

# copy saml2aws config
echo -e "\e[1;31m SAML2AWS COPY \e[0m"
cp -rf aws/.saml2aws $HOME/
