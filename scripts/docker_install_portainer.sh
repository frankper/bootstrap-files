#!/usr/bin/env bash
set -e

# Check if Docker is installed
if ! command -v docker &>/dev/null; then
  echo "Docker is not installed. Please install Docker before running this script."
  exit 1
fi

## Create a Docker volume for Portainer data if it doesn't exist
#if ! docker volume inspect portainer_data &>/dev/null; then
#  echo "Creating Docker volume 'portainer_data'..."
#  docker volume create portainer_data
#fi
#
## Remove existing Portainer container if it exists
#if docker ps -a --format '{{.Names}}' | grep -w "portainer" &>/dev/null; then
#  echo "Existing Portainer container found. Removing..."
#  docker rm -f portainer
#fi

# Run Portainer CE container
echo "Starting Portainer CE container..."
docker run -d \
  -p 8000:8000 \
  -p 9443:9443 \
  -p 9000:9000 \
  --name portainer \
  --restart=always \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v portainer_data:/data \
  portainer/portainer-ce:latest


echo "Portainer CE installation complete."


# Obtain the hostname from the environment or fallback to the hostname command
if [ -z "$HOSTNAME" ]; then
  HOSTNAME=$(hostname)
fi

echo "Access Portainer via https://$HOSTNAME:9000/"
