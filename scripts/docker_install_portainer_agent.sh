#!/usr/bin/env bash
set -e

# Check if Docker is installed
if ! command -v docker &>/dev/null; then
  echo "Docker is not installed. Please install Docker before running this script."
  exit 1
fi

# Run  Portainer Agent container
echo "Starting Portainer Agent container..."
docker run -d \
  -p 9001:9001 \
  --name portainer_agent \
  --restart=always \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v /var/lib/docker/volumes:/var/lib/docker/volumes \
  -v /:/host \
  portainer/agent:2.21.5


echo "Portainer Agent installation complete."

