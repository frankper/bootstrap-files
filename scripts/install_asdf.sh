#!/usr/bin/env bash

# Set variables
REPO="asdf-vm/asdf"
INSTALL_DIR="/usr/local/bin"

# Get latest release
LATEST_RELEASE=$(curl -s https://api.github.com/repos/$REPO/releases/latest | grep -oP '"tag_name": "\K(.*?)(?=")')
TAR_URL="https://github.com/$REPO/releases/download/$LATEST_RELEASE/asdf-$LATEST_RELEASE-linux-amd64.tar.gz"
echo "Downloading $TAR_URL..."
curl -L -o asdf.tar.gz "$TAR_URL"
echo "Extracting asdf.tar.gz..."
tar -xzf asdf.tar.gz


# Move files to /usr/local/bin
echo "Moving files to $INSTALL_DIR..."
sudo mv asdf $INSTALL_DIR

# check if asdf is installed
if ! command -v asdf &> /dev/null
then
    echo "asdf could not be found"
    exit
fi

# Cleanup
echo "Cleaning up"
rm asdf.tar.gz

# setup shell completions
echo "Setup zsh shell completions"
mkdir -p "${ASDF_DATA_DIR:-$HOME/.asdf}/completions"
asdf completion zsh > "${ASDF_DATA_DIR:-$HOME/.asdf}/completions/_asdf"

echo "asdf installed successfully in $INSTALL_DIR"
