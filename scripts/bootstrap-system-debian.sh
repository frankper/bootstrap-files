#!/usr/bin/env bash
# tested on D12
export TZ=Etc/UT
export LANG=C.UTF-8
export LC_ALL=C.UTF-8
export DEBIAN_FRONTEND=noninteractive
sudo apt-get update -qy && sudo apt-get -qy upgrade
xargs sudo apt-get install -qy < $HOME/utils/scripts/system_packages_to_install_debian.txt
sudo apt-get autoremove -qy && sudo apt-get clean -qy && sudo rm -rf /var/cache/apt/archives/
sudo chsh $USER -s $(which zsh)
