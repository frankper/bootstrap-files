#!/usr/bin/env bash
curl -LSfs https://raw.githubusercontent.com/byron/dua-cli/master/ci/install.sh | \
    sh -s -- --git byron/dua-cli --target x86_64-unknown-linux-musl --crate dua

echo -e "\e[1;31m DUA INSTALL DONE \e[0m"
