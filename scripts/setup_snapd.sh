#!/usr/bin/env bash 
# tested in ubuntu 22.04
# vars
COMP_NAME='snap packages'
unset  packagelist
# snap setup for multiple installs 
#sudo snap set system experimental.parallel-instances=true

declare packagelist1=(
    authy
    trello-desktop
    1password
    #notion-snap
    #multipass
    #beekeeper-studio
    #postman
    #obs-studio
    #session-desktop
    #signal-desktop
    #telegram-desktop
    #spotify
    winbox
    #rpi-imager
    #joplin-desktop
    #"kontena-lens --classic"
    #"zoom-client"
    #"slack --classic"
    #"code --classic"
    #"code-insiders --classic"
    #"sublime-text --classic"
    #"gitkraken --classic"
    #"powershell --classic"
    #"intellij-idea-community --classic"
    #"goland --classic"
    #"android-studio --classic"
    #"redis-desktop-manager"
    #drawio
)

# snap installs
for i in ${!packagelist1[@]}; do 
    sudo snap install ${packagelist1[i]}
done

