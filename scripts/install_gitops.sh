#!/usr/bin/env bash
curl --silent --location "https://github.com/weaveworks/weave-gitops/releases/download/v0.6.2/gitops-$(uname)-$(uname -m).tar.gz" | tar xz -C /tmp
sudo mv /tmp/gitops /usr/local/bin
gitops version
echo -e "\e[1;31m WEAVE GITOPS INSTALL DONE \e[0m"
