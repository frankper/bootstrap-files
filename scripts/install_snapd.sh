#!/usr/bin/env bash
 # vars
COMP_NAME=snapd
# install snapd 
sudo apt update -qy
sudo apt install -qy snapd
# verify snap 
sudo snap version
# test 
sudo snap install test-snapd-tools
test-snapd-tools.echo hello