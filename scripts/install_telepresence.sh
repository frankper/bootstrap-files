#!/usr/bin/env bash
# tested in debian 10 
#vars 
COMP_NAME=telepresence

# 1. Download the latest binary (~50 MB):
sudo curl -fL https://app.getambassador.io/download/tel2/linux/amd64/latest/telepresence -o /usr/local/bin/telepresence

# 2. Make the binary executable:
sudo chmod a+x /usr/local/bin/telepresence

which $COMP_NAME