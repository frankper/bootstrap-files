#!/usr/bin/env bash 
## Tested in arch
## based on this https://wiki.archlinux.org/title/Zram

sudo bash -c "echo 0 > /sys/module/zswap/parameters/enabled"
sudo swapoff --all
sudo modprobe zram num_devices=1
sudo zramctl /dev/zram0 --algorithm zstd --size 8G
sudo mkswap -U clear /dev/zram0
sudo swapon --priority 100 /dev/zram0
