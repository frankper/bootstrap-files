#!/usr/bin/env bash

# declare user groups  
declare -a UserGroups=(sudo docker wheel)
username=kfp
for group in ${UserGroups[@]}; do 
    usermod -aG $group $username
done