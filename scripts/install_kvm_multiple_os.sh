#!/usr/bin/env bash

# tested on ubuntu22.04,debian12 ,fedora39,opensuse,arch linux
#!/usr/bin/env bash
# timer start
start=$(date +%s.%N)
current_time=$(date +'%T-%d/%m/%Y')
script_name="Install KVM Multiple OS Script"
# Functions in use 
function colorprintf () {
    case $1 in
        "red") tput setaf 1;;
        "green") tput setaf 2;;
        "yellow") tput setaf 3;;
        "blue") tput setaf 4;;
        "purple") tput setaf 5;;
        "cyan") tput setaf 6;;
        "gray" | "grey") tput setaf 7;;
        "white") tput setaf 8;;
    esac
    echo "$2";
    tput sgr0
}

function ExitMessage () {
    duration=$(echo "$(date +%s.%N) - $start" | bc)
    # execution_time=`colorprintf  "%.2f seconds" $duration`
    colorprintf purple "${script_name} Done in $(colorprintf  "%.2f seconds" ${duration})"
    colorprintf green "Completed $script_name"
}

function installArch () {
    # check for pacman 
    if [ -f /usr/bin/pacman ]; then
        colorprintf yellow "Installing kvm on Arch Linux"
        sudo pacman -S qemu-full virt-manager virt-viewer dnsmasq bridge-utils libguestfs ebtables vde2 --noconfirm
    fi
}


function fedoraInstall () {
    # check for dnf
    if [ -f /usr/bin/dnf ]; then
        colorprintf yellow "Installing kvm on Fedora"
        sudo dnf install -y qemu-kvm libvirt virt-install bridge-utils virt-manager libvirt-devel virt-top libguestfs-tools guestfs-tools
    fi
}

function opensuseInstall () {
    # check for zypper
    if [ -f /usr/bin/zypper ]; then
        colorprintf yellow "Installing kvm on Opensuse"
        sudo zypper --non-interactive  --quiet install pattern kvm_server kvm_tools
    fi
}

function debianInstall () {
    # check for zypper
    if [ -f /usr/bin/Apt]; then
        colorprintf yellow "Installing kvm on Apt Based Linux"
        sudo apt -qy install vim libguestfs-tools libosinfo-bin  qemu-system virt-manager
    fi
}


function StartKvmServices () {
    # check for systemctl
    if [ -f /usr/bin/systemctl ]; 
    then
        colorprintf yellow "Starting KVM Services"
        sudo systemctl enable --now libvirtd.service
        # Check if vagrant is installed
    elif [[! command -v kvm &> /dev/null]];
    then
            echo "kvm could not be found"
            exit
    else
            echo "kvm has started successfully"
    fi
}


# I want to add 2 lines to the end of the file
# unix_sock_group = "libvirt"
# unix_sock_rw_perms = "0770"
# to file /etc/libvirt/libvirtd.conf
function AddLinesToFile () {
    # check if file exists
    if [ -f /etc/libvirt/libvirtd.conf ]; then
        colorprintf yellow "Adding lines to /etc/libvirt/libvirtd.conf"
        sudo tee -a /etc/libvirt/libvirtd.conf <<EOF
        unix_sock_group = "libvirt"
        unix_sock_rw_perms = "0770"
EOF
    fi
}


DetectOS () {
    unset VER
    unset OS
    unset DISTRIB_ID
    if [ -f /etc/lsb-release ]; then
        # Check if DISTRIB_ID is not 'Ubuntu'
        if ! grep -q "^DISTRIB_ID=Ubuntu" /etc/lsb-release; then
            echo "The file exists, but DISTRIB_ID is not 'Ubuntu'. Probably EndeavourOs Arch Linux"
            installArch
        else
            echo "The file exists and DISTRIB_ID is 'Ubuntu'."
            # For some versions of Debian/Ubuntu without lsb_release command
            . /etc/lsb-release
            OS=$DISTRIB_ID
            VER=$DISTRIB_RELEASE
            colorprintf yellow "OS detected: $OS $VER"
            debianInstall
        fi
    elif [ -f /etc/debian_version ]; then
        # Older Debian/Ubuntu/etc.
        OS=Debian
        VER=$(cat /etc/debian_version)
        colorprintf purple "OS detected: $OS $VER"
        if [[ "$VER" =~ .*"/".* ]]; then
            unset VER
            VER=bookworm_sid
        fi
        debianInstall
    elif [ -f /etc/os-release ]; then
        # opensuse 
        . /etc/os-release
        OS=$ID
        VER=$VERSION_ID
        colorprintf purple "OS detected: $OS $VER"
        opensuseInstall
    elif [ -f /etc/redhat-release ]; then
        . /etc/os-release
        OS=$NAME
        VER=$VERSION_ID
        colorprintf purple "OS detected: $OS $VER"
        fedoraInstall
    elif [ -f "/etc/arch-release" ]; then
        OS=Arch
        colorprintf purple "OS detected: $OS $VER"
        installArch
    else
        # Fall back to uname, e.g. "Linux <version>", also works for BSD, etc.
        OS=$(uname -s)
        VER=$(uname -r)
        # > $backup_location/installed_packages_${OS}_${VER}.txt
        colorprintf red "fb detected: $OS $VER not done the work on this yet..."
    fi
}

# check if kvm is installed
if ! command -v kvm &> /dev/null
then
    echo "kvm could not be found"
    echo "Installing kvm"
    DetectOS
    AddLinesToFile
    StartKvmServices
    ExitMessage
fi