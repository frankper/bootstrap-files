#!/usr/bin/env bash

echo -e "\e[1;31m DOCKER INSTALL START \e[0m"

curl -fsSL https://get.docker.com | sh -s
sudo usermod -aG docker $USER
sudo systemctl enable --now docker
sudo systemctl start docker
sudo newgrp docker

echo -e "\e[1;31m DOCKER INSTALL DONE \e[0m"
