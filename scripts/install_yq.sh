#!/usr/bin/env bash
VERSION=v4.2.0
BINARY=yq_linux_amd64
sudo wget -qO /usr/local/bin/yq https://github.com/mikefarah/yq/releases/latest/download/${BINARY} &&\
    sudo chmod +x /usr/local/bin/yq
echo -e "\e[1;31m YQ INSTALL DONE \e[0m"
