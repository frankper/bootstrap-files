#!/usr/bin/env bash
# tested in debian 10 
declare krewAddonList=(
  ctx
  ns
  fuzzy
  ktop
  kubectl-topology
  kubectl-free
  kubecolor
  kubectl-watch
  kubectl-clogs
  kubectl-clogs
  get-all
  status
  pod-dive
  janitor
  cost
  doctor
  flame
  fleet
  graph
  konfig
  virt
  tree
  stern
  oidc-login
  blame
  explore
  cilium
  kc
  konfig
  ktop
  kubesec-scan
  kubescape
  kurt
  mtail
  kvaps/node-shell
  oomd
  outdated
  pod-lens
  kail
  kubetap
  tmux-exec
  trace
  ktunnel
  view-utilization
  warp
  whoami
  who-can
)

declare krewIndexList=(
  "kvaps https://github.com/kvaps/krew-index"
)

function installKrewIndexs() {
for i in ${!krewIndexList[@]}; do 
    kubectl krew index add ${krewIndexList[i]}
done
}

function InstallKrewAddons() {
for i in ${!krewAddonList[@]}; do 
    kubectl krew install ${krewAddonList[i]}
done
}

source $HOME/.zshrc
kubectl krew update
kubectl krew upgrade
installKrewIndexs
InstallKrewAddons
kubectl krew list
