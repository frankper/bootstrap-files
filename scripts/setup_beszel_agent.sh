#!/usr/bin/env bash 


echo "Installing Beszel Agent..."
curl -sL https://raw.githubusercontent.com/henrygd/beszel/main/supplemental/scripts/install-agent.sh -o install-agent.sh && chmod +x install-agent.sh && ./install-agent.sh -p 45876 -k "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAcRWCz8NI5/XPhxYVPsKWOS2wyhwujYRrj06Ihtg2Gn"
echo "Beszel Agent installation complete."

# pull the hostname from the environment or use the hostname command as a fallback
if [ -z "$HOSTNAME" ]; then
  HOSTNAME=$(hostname)
fi

echo "Access Beszel Agent via https://$HOSTNAME:45876/"
