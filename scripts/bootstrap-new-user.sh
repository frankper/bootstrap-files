#!/usr/bin/env bash
# create a new used on linux ubuntu and add him on the sudoers file and add him to groups docker and sudo 

# declare user groups  
declare -a UserGroups=(sudo docker)

if [ $(id -u) -eq 0 ]; then
	read -p "Enter username : " username
	read -s -p "Enter password : " password
	egrep "^$username" /etc/passwd >/dev/null
	if [ $? -eq 0 ]; then
		echo "$username exists!"
		exit 1
	else
		pass=$(perl -e 'print crypt($ARGV[0], "password")' $password)
		useradd -m -p "$pass" "$username"
		[ $? -eq 0 ] && echo "User has been added to system!" || echo "Failed to add a user!"
        echo "$username ALL=(ALL) PASSWD: ALL " | sudo tee -a /etc/sudoers
        for group in ${UserGroups[@]}; do 
            usermod -aG $group $username
        done
	fi
else
	echo "Only root may add a user to the system."
	exit 2
fi