#!/usr/bin/env bash 
## Tested in ubuntu 22.04 , D12
## Based on https://www.lazyvim.org/installation

# Backup existing neovim conf

# required
mv $HOME/.config/nvim{,.bak}

# optional but recommended
mv $HOME/.local/share/nvim{,.bak}
mv $HOME/.local/state/nvim{,.bak}
mv $HOME/.cache/nvim{,.bak}

# Install Lazyvim conf
git clone https://github.com/LazyVim/starter $HOME/.config/nvim
rm -rf $HOME/.config/nvim/.git

