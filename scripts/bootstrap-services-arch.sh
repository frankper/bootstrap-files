#!/usr/bin/env bash
declare serviceslist1=(
    earlyoom
    tailscaled
    docker
)

function BootstrapServices() {
for i in ${!serviceslist1[@]}; do 
    sudo systemctl start ${serviceslist1[i]}
    sudo systemctl enable --now ${serviceslist1[i]}
done
}

BootstrapServices