#!/usr/bin/env bash 
# tested in ubuntu 22.04,debian 12,arch

## flatpak repos
sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
#sudo flatpak remote-add --from org.mozilla.FirefoxRepo https://firefox-flatpak.mojefedora.cz/org.mozilla.FirefoxRepo.flatpakrepo

declare packagelist=(
    flathub com.stremio.Stremio
    flathub org.telegram.desktop 
    flathub com.spotify.Client
    flathub us.zoom.Zoom
    flathub com.slack.Slack
    #flathub io.atom.Atom
    #flathub com.getpostman.Postman
    #flathub com.sublimetext.three
    #flathub com.bitwarden.desktop
    #flathub org.keepassxc.KeePassXC
    #flathub com.obsproject.Studio
    flathub net.cozic.joplin_desktop
    #flathub com.jgraph.drawio.desktop
    #flathub net.sourceforge.Hugin
    #flathub org.darktable.Darktable
    #flathub com.jetbrains.IntelliJ-IDEA-Community
    #flathub com.visualstudio.code
    #flathub org.chromium.Chromium
    flathub org.signal.Signal
    #flathub uk.co.ibboard.cawbird
    #flathub com.dangeredwolf.ModernDeck
    flathub com.github.zadam.trilium
    flathub hu.irl.cameractrls
    flathub com.github.tchx84.Flatseal
    #flathub md.obsidian.Obsidian
    flathub com.github.louis77.tuner
    flathub de.haeckerfelix.Shortwave
    #flathub io.github.shiftey.Desktop #github desktop
    flathub tv.plex.PlexDesktop
    flathub com.plexamp.Plexamp
    #flathub com.amazon.Workspaces
    flathub de.haeckerfelix.Fragments
)

## flatpak installs 
# flatbub
flatpak install ${packagelist[@]} -y


# mozilla unofficial
#flatpak install org.mozilla.FirefoxRepo org.mozilla.FirefoxNightly -y 
#flatpak install org.mozilla.FirefoxRepo org.mozilla.FirefoxDevEdition -y 


# flatpak install  org.mozilla.firefox -y
