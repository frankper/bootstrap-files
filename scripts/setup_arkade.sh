#!/usr/bin/env bash
# tested in debian 10
declare packagelist1=(
  #kubectl
  #helm
  #istioctl
  #clusterctl
  #doctl
  #kind
  #faas-cli
)

function InstallArkadeAddons() {
for i in ${!packagelist1[@]}; do 
    ark get ${packagelist1[i]}
done
}

source $HOME/.zshrc
InstallArkadeAddons
