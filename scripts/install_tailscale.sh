#!/usr/bin/env bash
# Do not use with arch linux
# tested with Ubunt 22.04 , Fedora 38

curl -fsSL https://tailscale.com/install.sh | sh

# check if tailscale is installed
if ! command -v tailscale &> /dev/null
then
    echo "tailscale could not be found"
    exit
fi

# attempt to start tailscale service
echo "Starting tailscale service"
sudo systemctl start tailscaled

# check if tailscaled is running
if ! pgrep -x "tailscaled" > /dev/null
then
    echo "tailscale service did not start successfully"
    exit
fi

# set tailscale auto update 
if ! command -v tailscale &> /dev/null
then
    echo "tailscale could not be found"
    exit
else 
    echo "Setting tailscale to auto update"
    sudo tailscale set --auto-update
fi