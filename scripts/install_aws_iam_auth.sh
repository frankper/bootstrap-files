#!/usr/bin/env bash
# vars
COMP_NAME=aws-iam-authenticator
FILE_EXT='2021-07-05'
COMP_VERSION=1.21.2
APP_DIR='/usr/local/bin'
TEMP_DIR='/tmp'

curl -Lo ${TEMP_DIR}/${COMP_NAME} https://amazon-eks.s3.us-west-2.amazonaws.com/${COMP_VERSION}/${FILE_EXT}/bin/linux/amd64/aws-iam-authenticator && \
sudo chmod +x ${TEMP_DIR}/${COMP_NAME} && \
sudo mv ${TEMP_DIR}/${COMP_NAME} ${APP_DIR}

which ${COMP_NAME}
