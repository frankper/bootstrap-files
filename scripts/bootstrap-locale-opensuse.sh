#!/usr/bin/env bash
# on Tumbleeweed 20231108 
# Generate UK UTF-8 locale
echo "Generating en_GB.UTF-8 locale..."
sudo localectl set-locale en_GB.UTF-8

# Update the system locale configuration
echo "Updating system locale configuration..."
sudo localectl set-locale LANG=en_GB.UTF-8

# Display a message indicating that locale generation is complete
echo "UK UTF-8 locale generation completed."
