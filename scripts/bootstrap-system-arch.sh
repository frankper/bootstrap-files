#!/usr/bin/env bash
export TZ=Etc/UT
export LANG=C.UTF-8
sudo locale-gen
sudo pacman --sync --refresh
sudo pacman --sync --needed archlinux-keyring
sudo pacman -Syu --noconfirm
sudo pacman -S $(awk '{print $1}' utils/scripts/system_packages_to_install_arch.txt) --noconfirm
sudo chsh $USER -s $(which zsh)