#!/usr/bin/env bash
# tested on Tumbleeweed 20231108 
sudo zypper --non-interactive --quiet ref && sudo zypper --non-interactive --quiet dup
sudo zypper --non-interactive  --quiet patch
sudo zypper --non-interactive  --quiet install $(cat $HOME/utils/scripts/system_packages_to_install_opensuse.txt)
sudo chsh $USER -s $(which zsh)
