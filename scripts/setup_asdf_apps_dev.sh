#!/usr/bin/env bash
unset  HashicorpPluginsToInstall PluginsToInstall SoftVersions
# vars
COMP_NAME=asdf

declare -a SoftVersions=(
    #"neovim latest"
    #"golang 1.17.5"
    #"golang 1.16.10"
    "eksctl latest"
    "kustomize latest"
    "k9s latest"
    #"krew latest"
    #"direnv latest"
    "helm latest"
    "kubetail latest"
    #"docker-compose latest"
    "kubectl latest"
    #"kubectl 1.15.0"
    #"kubectl 1.25.0"
    "helm latest"
    "packer latest"
    "terraform latest"
    "opentofu latest"
    #"terraform 0.11.11"
    #"terraform 0.12.21"
    #"terraform 0.13.6"
    #"terraform 0.14.7"
    #"terraform 0.15.1"
    #"terraform 1.0.0"
    #"vault latest"
    #"vault 1.3.2"
    #"nodejs latest"
    #"nodejs 12.22.0"
    #"tflint latest"
    #"tflint 0.23.1"
    #"golang latest" 
    #"golang 1.17.1"
    #"golang 1.17.5"
    #"golang 1.16.10"
    "pulumi latest"
    #"tmux latest"
    "awscli latest"
    "kind latest"
    #"kind 0.11.1"
    #"hugo latest"
    #"yarn latest"
    "gcloud latest"
    #"lazydocker latest"
    #"teleport-ent 11.3.26"
    #"helmfile latest"
    #"cilium-hubble latest"
    #"terragrunt latest"
    #"cilium-cli latest"
    #"trivy latest"\
    #"minikube latest"
    #"yq latest"
    #"delta latest"
    #"istio latest"
    #"doctl latest"
    #"clusterctl latest"
    #"bat latest"
    #"eza latest"
    "azure-cli latest"
)

# source $HOME/.zshrc
#source $HOME/.zshrc

# adsf 
export PATH="${ASDF_DATA_DIR:-$HOME/.asdf}/shims:$PATH"
fpath=(${ASDF_DATA_DIR:-$HOME/.asdf}/completions $fpath)
#autoload -Uz compinit && compinit

# Check if asdf is installed
if ! command -v asdf &> /dev/null
then
    echo "asdf could not be found"
    exit
fi

# Asdf software version installs
for x in ${!SoftVersions[@]}; do 
    asdf install ${SoftVersions[x]}
    asdf set ${SoftVersions[x]}
done

## Deprecated after asdf upgraded to v0.16 https://asdf-vm.com/guide/upgrading-to-v0-16#breaking-changes 
## Asdf define global software versions
#for x in ${!SoftVersions[@]}; do 
#    asdf global ${SoftVersions[x]}
#done
