#!/usr/bin/env bash
# Tested in F38

## Check if the script is run with root privileges
#if [ "$EUID" -ne 0 ]; then
#  echo "Please run this script as root or using sudo."
#  exit 1
#fi

# Install the required package if not already installed
if ! rpm -q glibc-langpack-en >/dev/null 2>&1; then
  echo "Installing glibc-langpack-en..."
  sudo dnf install -y glibc-langpack-en
fi

# Generate UK UTF-8 locale
echo "Generating en_GB.UTF-8 locale..."
sudo locale-gen en_GB.UTF-8

# Update the system locale configuration
echo "Updating system locale configuration..."
sudo localectl set-locale LANG=en_GB.UTF-8

# Display a message indicating that locale generation is complete
echo "UK UTF-8 locale generation completed."
