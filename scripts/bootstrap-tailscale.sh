#!/usr/bin/env bash

# Check if tailscale is installed
if ! command -v tailscale &> /dev/null
then
    echo "tailscale could not be found"
    exit
fi

# check if tailscaled is running
if ! pgrep -x "tailscaled" > /dev/null
then
    echo "tailscale service is not running"
    exit
fi

# check if a file is txt or binary and if it is a text file, then run commands 
# https://stackoverflow.com/questions/9910454/check-if-a-file-is-text-or-binary-in-bash-without-using-file-command
function run_if_text_file() {
    local file="$1"
    local type="$(file -b --mime-encoding "$file")"
    if [[ "$type" == utf-8 || "$type" == us-ascii ]]; then
        echo "Registering Tailscale"
        cat $1 | sudo tailscale up --authkey `xargs` --ssh --advertise-tags=tag:dev,tag:vm
        sudo tailscale set --auto-update
        echo "Tailscale is registered"
    else
        echo "Token file is still a binary , unlock it with git-crypt, skipping tailscale registration"
    fi
}

run_if_text_file $HOME/utils/private/tailscale.token

