#!/usr/bin/env bash

# GoLang
export GOROOT=~/.go
export PATH=$GOROOT/bin:$PATH
export GOPATH=~/go
export PATH=$GOPATH/bin:$PATH

go get -u github.com/schachmat/wego
go get -u github.com/owenthereal/ccat
echo -e "\e[1;31m GO PACKAGES INSTALL DONE \e[0m"
