#!/usr/bin/env bash
# tested on alpine 3.19

doas apk update && doas apk upgrade
xargs doas apk add --no-cache < $HOME/utils/scripts/system_packages_to_install_alpine.txt
doas rm -rf /var/cache/apk/*
doas chsh $USER -s $(which zsh)
