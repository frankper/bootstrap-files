#!/usr/bin/env bash
# timer start
start=$(date +%s.%N)
current_time=$(date +'%T-%d/%m/%Y')
script_name="Update My System Script"
# Functions in use 
function colorprintf () {
    case $1 in
        "red") tput setaf 1;;
        "green") tput setaf 2;;
        "yellow") tput setaf 3;;
        "blue") tput setaf 4;;
        "purple") tput setaf 5;;
        "cyan") tput setaf 6;;
        "gray" | "grey") tput setaf 7;;
        "white") tput setaf 8;;
    esac
    echo "$2";
    tput sgr0
}

function DeleteAptLock (){
    sudo rm /var/lib/apt/lists/lock
    sudo rm /var/cache/apt/archives/lock
    sudo rm /var/lib/dpkg/lock
}

function UpdateApt () {
    colorprintf yellow "Update Apt Packages"
    sudo apt -qqy update
    colorprintf yellow "Upgrade Apt Packages"
    sudo apt -qqy full-upgrade
    colorprintf yellow "Autoremove Apt Packages"
    sudo apt -qqy autoremove 
    colorprintf yellow "Autoclean Apt Packages"
    sudo apt -qqy autoclean
    colorprintf green "Updated Apt Packages"
}

function UpdateSnap () {
    colorprintf yellow "Update Snap Packages"
    sudo snap refresh
    colorprintf green "Updated Snap Packages"
}

function UpdateFlat () {
    colorprintf yellow "Update Flatpak Packages"
    flatpak update -y
    colorprintf green "Updated Flatpak Packages"
}

function UpdateGnomeExtensions () {
    colorprintf yellow "Update Gnome Extensions"
    gnome-shell-extension-installer --yes --update --restart-shell
    colorprintf green "Updated Gnome Extensions"
    colorprintf yellow "Try new Update Gnome Extensions tool ( gext https://github.com/essembeh/gnome-extensions-cli )"
    gext update
    colorprintf green "Updated Gnome Extensions"
}

function ExitMessage () {
    duration=$(echo "$(date +%s.%N) - $start" | bc)
    # execution_time=`colorprintf  "%.2f seconds" $duration`
    colorprintf purple "${script_name} Done in $(colorprintf  "%.2f seconds" ${duration})"
    colorprintf green "Completed $script_name"
}

function UpdatePacman () {
    colorprintf yellow "Create list of installed packages before update"
    # generate list of installed packages 
    today=`date +%d-%m-%Y.%H:%M:%S`
    sudo pacman -Qqe > $HOME/Documents/Pacman_list_of_installed_software_pre_update_${today}.txt

    colorprintf yellow "Refresh Pacman Repos"
    sudo pacman --sync --refresh

    colorprintf yellow "Refresh Keyrings"
    sudo pacman --sync --needed archlinux-keyring
    sudo pacman --sync --needed endeavouros-keyring

    colorprintf yellow "Update Pacman Packages"
    #sudo pacman -Syu --noconfirm
    #temp test to change
    #sudo pacman -Sy --noconfirm && sudo powerpill -Su --noconfirm && paru -Su --noconfirm
    sudo pacman -Sy --noconfirm && paru -Su --noconfirm
    #sudo pacman --sync --sysupgrade
    # endeavouros update
    eos-update
    
    # Vanilla Arch server 
    sudo pacman -Suy --noconfirm

    colorprintf yellow "Remove Orphan Packages"
    # Removing All Orphans If there are many orphans, you can remove all of them with the following command:
    pacman -Qtdq | sudo pacman -Rns -

    colorprintf yellow "Create list of installed packages after update"
    # generate list of installed packages 
    today=`date +%d-%m-%Y.%H:%M:%S`
    sudo pacman -Qqe > $HOME/Documents/Pacman_list_of_installed_software_after_update_${today}.txt
}

function DebianBasedUpdate () {
    DeleteAptLock
    UpdateApt
}

function ArchBasedUpdate () {
    UpdatePacman
}

function FedoraUpdate () {
    echo "Updating Fedora..."
    sudo dnf update -y && sudo dnf autoremove -y
}


function OpensuseUpdate () {
    echo "Updating Opensuse..."
    sudo zypper --non-interactive --quiet ref && sudo zypper --non-interactive --quiet dup
    sudo zypper --non-interactive  --quiet patch
}

function UpdateRandom () {
    # omz 
    colorprintf yellow "Update OMZ"
    omz update
    # homebrew
    colorprintf yellow "Update brew"
    brew update && brew upgrade
    # krew 
    colorprintf yellow "Update krew plugins"
    kubectl krew upgrade
    # zfz
    # cd $HOME/.fzf && git pull && ./install --all
}

function UpdateNormal () {
    UpdateSnap
    UpdateFlat
    UpdateGnomeExtensions
    UpdateRandom
}

DetectOS () {
    unset VER
    unset OS
    unset DISTRIB_ID
    if [ -f /etc/lsb-release ]; then
        # For some versions of Debian/Ubuntu without lsb_release command
        . /etc/lsb-release
        OS=$DISTRIB_ID
        VER=$DISTRIB_RELEASE
        colorprintf yellow "OS detected: $OS $VER"
    elif [ -f /etc/debian_version ]; then
        # Older Debian/Ubuntu/etc.
        OS=Debian
        VER=$(cat /etc/debian_version)
        colorprintf purple "OS detected: $OS $VER"
        if [[ "$VER" =~ .*"/".* ]]; then
            unset VER
            VER=bookworm_sid
        fi
    elif [ -f /etc/os-release ]; then
        # opensuse 
        . /etc/os-release
        OS=$ID
        VER=$VERSION_ID
        colorprintf purple "OS detected: $OS $VER"
    elif [ -f /etc/redhat-release ]; then
        . /etc/os-release
        OS=$NAME
        VER=$VERSION_ID
        colorprintf purple "OS detected: $OS $VER"
    elif [ -f "/etc/arch-release" ]; then
        OS=Arch
        colorprintf purple "OS detected: $OS $VER"
    else
        # Fall back to uname, e.g. "Linux <version>", also works for BSD, etc.
        OS=$(uname -s)
        VER=$(uname -r)
        # > $backup_location/installed_packages_${OS}_${VER}.txt
        colorprintf red "fb detected: $OS $VER not done the work on this yet..."
    fi
}

colorprintf green "Running $script_name"
# functions to run

DetectOS

# Check for Debian

if [ -f /etc/debian_version ]; then
    DebianBasedUpdate
fi

# Check for Ubuntu based distro
if [ -f /etc/lsb-release ]; then
    DebianBasedUpdate
fi

# Check for Fedora
if [ -f /etc/fedora-release ]; then
    FedoraUpdate
fi

# Check for Opensuse
if [ -f /etc/os-release ]; then
    OpensuseUpdate
fi

# Check for Arch Linux
if [ -f /etc/arch-release ]; then
    ArchBasedUpdate
fi

UpdateNormal
ExitMessage
