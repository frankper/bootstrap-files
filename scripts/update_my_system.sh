# update script V2 04.01.2024 
# tested on ubuntu22.04,debian12 ,fedora39,opensuse,arch linux
#!/usr/bin/env bash
# timer start
start=$(date +%s.%N)
current_time=$(date +'%T-%d/%m/%Y')
script_name="Update My System Script"
script_folder=$(pwd)
# Functions in use 
function colorprintf () {
    case $1 in
        "red") tput setaf 1;;
        "green") tput setaf 2;;
        "yellow") tput setaf 3;;
        "blue") tput setaf 4;;
        "purple") tput setaf 5;;
        "cyan") tput setaf 6;;
        "gray" | "grey") tput setaf 7;;
        "white") tput setaf 8;;
    esac
    echo "$2";
    tput sgr0
}

function DeleteAptLock (){
    # check for apt
    if [ -f /usr/bin/apt ]; then
        sudo rm /var/lib/apt/lists/lock
        sudo rm /var/cache/apt/archives/lock
        sudo rm /var/lib/dpkg/lock
    fi
}

function UpdateApt () {
    # check for apt
    if [ -f /usr/bin/apt ]; then
        colorprintf yellow "Update Apt Packages"
        sudo apt -qqy update
        colorprintf yellow "Upgrade Apt Packages"
        sudo apt -qqy full-upgrade
        colorprintf yellow "Autoremove Apt Packages"
        sudo apt -qqy autoremove 
        colorprintf yellow "Autoclean Apt Packages"
        sudo apt -qqy autoclean
        colorprintf green "Updated Apt Packages"
    fi
}

function UpdateAptNala () {
    # check for nala 
    if [ -f /usr/bin/nala ]; then
        colorprintf yellow "Update Apt Packages using Nala"
        # update system using nala
        sudo nala upgrade -y
        sudo nala autoremove -y
        sudo nala clean
    fi
}

function UpdateApkAlpine () {
    # check for apk/Alpine 
    if [ -f /usr/bin/apk ]; then
        colorprintf yellow "Update apk packages"
        doas apk update
        doas apk upgrade
        doas rm -rf /var/cache/apk/*
    fi
}

function UpdateSnap () {
    # check for snap
    if [ -f /usr/bin/snap ]; then
        colorprintf yellow "Update Snap Packages"
        sudo snap refresh
        colorprintf green "Updated Snap Packages"
    fi
}

function UpdateFlatpak () {
    # check for flatpak
    if [ -f /usr/bin/flatpak ]; then
        colorprintf yellow "Update Flatpak Packages"
        flatpak update -y
        colorprintf green "Updated Flatpak Packages"
        colorprintf yellow "Cleanup/Repair Flatpak Packages"
        flatpak uninstall --unused -y 
        flatpak repair
        colorprintf yellow "Cleaned up Flatpak Packages"  
    fi
}

function UpdateGnomeExtensions () {
    # check for gnome-shell-extension-installer
    if [ -f /usr/bin/gnome-shell-extension-installer ]; then
        colorprintf yellow "Update Gnome Extensions"
        gnome-shell-extension-installer --yes --update --restart-shell
        colorprintf green "Updated Gnome Extensions"
    fi
    # check for gext
    if [ -f /home/kfp/.local/bin/gext ]; then
        colorprintf yellow "Update Gnome Extensions"
        gext update
        colorprintf green "Updated Gnome Extensions"
    fi
}

function ExitMessage () {
    duration=$(echo "$(date +%s.%N) - $start" | bc)
    # execution_time=`colorprintf  "%.2f seconds" $duration`
    colorprintf purple "${script_name} Done in $(colorprintf  "%.2f seconds" ${duration})"
    colorprintf green "Completed $script_name"
}

function UpdatePacman () {
    # check for pacman 
    if [ -f /usr/bin/pacman ]; then
        colorprintf yellow "Create list of installed packages before update"
        # generate list of installed packages 
        today=`date +%d-%m-%Y.%H:%M:%S`
        sudo pacman -Qqe > $HOME/Documents/Pacman_list_of_installed_software_pre_update_${today}.txt

        colorprintf yellow "Refresh Pacman Repos"
        sudo pacman --sync --refresh

        colorprintf yellow "Refresh Keyrings"
        sudo pacman --sync --needed archlinux-keyring

        colorprintf yellow "Update Pacman Packages"
        colorprintf yellow "Remove Orphan Packages"
        # Removing All Orphans If there are many orphans, you can remove all of them with the following command:
        sudo pacman -Qtdq | sudo pacman -Rns -

        colorprintf yellow "Create list of installed packages after update"
        # generate list of installed packages 
        today=`date +%d-%m-%Y.%H:%M:%S`
        sudo pacman -Qqe > $HOME/Documents/Pacman_list_of_installed_software_after_update_${today}.txt
    fi

    # check for paru
    if [ -f /usr/bin/paru ]; then
        colorprintf yellow "Update Packaged using Pacman + Paru "
        sudo pacman -Sy --noconfirm && paru -Su --noconfirm
    fi

    # check for eos-update 
    if [ -f /usr/bin/eos-update ]; then
        colorprintf yellow "Update EndeavourOS"
        sudo pacman --sync --needed endeavouros-keyring
        eos-update
    fi
}

function DebianBasedUpdate () {
    DeleteAptLock
    # check for nala 
    if [ -f /usr/bin/nala ]; then
        UpdateAptNala
    else
        UpdateApt
    fi
}

function ArchBasedUpdate () {
    UpdatePacman
}

function FedoraUpdate () {
    # check for dnf
    if [ -f /usr/bin/dnf ]; then
        colorprintf yellow "Update Fedora"
        sudo dnf update -y && sudo dnf autoremove -y
    fi
}

function OpensuseUpdate () {
    # check for zypper
    if [ -f /usr/bin/zypper ]; then
        colorprintf yellow "Update Opensuse"
        sudo zypper --non-interactive --quiet ref && sudo zypper --non-interactive --quiet dup
        sudo zypper --non-interactive  --quiet patch
    fi
}

function UpdateOmz () {
    # omz
    # check if a folder exists ~/.oh-my-zsh
    if [ -d "$HOME/.oh-my-zsh" ]; then
        colorprintf yellow "Update OMZ"
        omz update
    fi

}

function UpdateKrew () {
    # check for krew
    # Try to list kubectl plugins using Krew
    kubectl krew list > /dev/null 2>&1
    # Check if the previous command was successful
    if [ $? -eq 0 ]; then
        colorprintf yellow "Update krew plugins"
        kubectl krew upgrade > /dev/null 2>&1
        kubectl krew list
    fi
}

## Deprecated after asdf upgraded to v0.16 https://asdf-vm.com/guide/upgrading-to-v0-16#breaking-changes 
#function UpdateAsdf () {
#    # check for asdf
#    if [ -f $HOME/.asdf/bin/asdf ]; then
#        colorprintf yellow "Update asdf"
#        asdf update
#    fi
#}

function UpdateHomebrew () {
    # check for brew
    brew --version > /dev/null 2>&1
    if [ $? -eq 0 ]; then
        colorprintf yellow "Update brew"
        # update system using brew
        brew update 
        brew upgrade
        brew autoremove
        brew cleanup --prune=all
    fi
}

function Updatefzf () {
    # check for fzf
    if [ -f $HOME/.fzf/bin/fzf ]; then
        colorprintf yellow "Update fzf"
        cd $HOME/.fzf && git pull > /dev/null 2>&1
        if [ $? -eq 0 ]; then
            colorprintf green "fzf Git pull was successful"
            $HOME/.fzf/install --all --completion --key-bindings --no-update-rc   
        else
            colorprintf red "fzf Git pull failed"
        fi
        cd $script_folder
    fi
}

function UpdateDevbox () {
    # check for devbox
    devbox version > /dev/null 2>&1
    if [ $? -eq 0 ]; then
        colorprintf yellow "Update devbox"
        devbox version update
        devbox update
        devbox global update
    fi
}

function UpdatePipx () {
    # check for pipx
    if [ -f /bin/pipx ]; then
        colorprintf yellow "Update pipx"
        pipx upgrade-all
    fi
}

function UpdateExtra () {
    UpdateSnap
    UpdateFlatpak
    UpdateGnomeExtensions
    UpdateHomebrew
    UpdateAsdf
    Updatefzf
    UpdateKrew
    UpdateOmz
    # UpdateDevbox # some builds with devbox take too long to complete so will be using this as an alias manually 
}

DetectOS () {
    unset VER
    unset OS
    unset DISTRIB_ID
    if [ -f /etc/lsb-release ]; then
        # For some versions of Debian/Ubuntu without lsb_release command
        . /etc/lsb-release
        OS=$DISTRIB_ID
        VER=$DISTRIB_RELEASE
        colorprintf yellow "OS detected: $OS $VER"
    elif [ -f /etc/debian_version ]; then
        # Older Debian/Ubuntu/etc.
        OS=Debian
        VER=$(cat /etc/debian_version)
        colorprintf purple "OS detected: $OS $VER"
        if [[ "$VER" =~ .*"/".* ]]; then
            unset VER
            VER=bookworm_sid
        fi
    elif [ -f /etc/os-release ]; then
        # opensuse 
        . /etc/os-release
        OS=$ID
        VER=$VERSION_ID
        colorprintf purple "OS detected: $OS $VER"
    elif [ -f /etc/redhat-release ]; then
        . /etc/os-release
        OS=$NAME
        VER=$VERSION_ID
        colorprintf purple "OS detected: $OS $VER"
    elif [ -f "/etc/arch-release" ]; then
        OS=Arch
        colorprintf purple "OS detected: $OS $VER"
    elif [ -f "/etc/alpine-release" ]; then
        OS=Alpine
        VER=$(cat /etc/alpine-release)
        colorprintf purple "OS detected: $OS $VER"
    else
        # Fall back to uname, e.g. "Linux <version>", also works for BSD, etc.
        OS=$(uname -s)
        VER=$(uname -r)
        # > $backup_location/installed_packages_${OS}_${VER}.txt
        colorprintf red "fb detected: $OS $VER not done the work on this yet..."
    fi
}

colorprintf green "Running $script_name"
# functions to run

DetectOS

# Check for Debian

if [ -f /etc/debian_version ]; then
    DebianBasedUpdate
fi

# Check for Ubuntu based distro
if [ -f /etc/lsb-release ]; then
    DebianBasedUpdate
fi

# Check for Fedora
if [ -f /etc/fedora-release ]; then
    FedoraUpdate
fi

# Check for Opensuse
if [ -f /etc/os-release ]; then
    OpensuseUpdate
fi

# Check for Arch Linux
if [ -f /etc/arch-release ]; then
    ArchBasedUpdate
fi

# Check for Alpine
if [ -f /etc/alpine-release ]; then
    UpdateApkAlpine
fi

UpdateExtra
ExitMessage
