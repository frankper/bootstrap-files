#!/usr/bin/env bash
mkdir -p $HOME/.config/tmuxinator/
mkdir -p $HOME/.config/htop/
cp $HOME/utils/dotfiles/starship/starship.toml $HOME
cp $HOME/utils/dotfiles/zsh/.zsh_profile_alias $HOME
cp $HOME/utils/dotfiles/zsh/.zsh_profile_linux $HOME
cp $HOME/utils/dotfiles/zsh/.zsh_history $HOME
cp $HOME/utils/dotfiles/zsh/.zshrc $HOME
cp $HOME/utils/dotfiles/bash/.bash_logout $HOME
cp $HOME/utils/dotfiles/bash/.bash_profile $HOME
cp $HOME/utils/dotfiles/bash/.bashrc $HOME
cp $HOME/utils/dotfiles/asdf/.tool-versions $HOME
cp $HOME/utils/dotfiles/asdf/.asdfrc $HOME
touch $HOME/utils/dotfiles/vim/init.toml $HOME/.SpaceVim.d/init.toml
cp $HOME/utils/dotfiles/vim/init.toml $HOME/.SpaceVim.d/init.toml
cp $HOME/utils/dotfiles/git/.gitconfig $HOME
touch $HOME/utils/dotfiles/tmuxinator/main.yml $HOME/.config/tmuxinator/
cp $HOME/utils/dotfiles/tmuxinator/main.yml $HOME/.config/tmuxinator/
cp $HOME/utils/dotfiles/htop/htoprc $HOME/.config/htop/