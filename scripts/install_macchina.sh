#!/usr/bin/env bash

# Set variables
REPO="Macchina-CLI/macchina"
INSTALL_DIR="/usr/local/bin"

# Get latest release tag
LATEST_RELEASE=$(curl -s https://api.github.com/repos/${REPO}/releases/latest | grep -oP '"tag_name": "\K(.*?)(?=")')

# Construct download URL
TAR_URL="https://github.com/${REPO}/releases/download/${LATEST_RELEASE}/macchina-${LATEST_RELEASE}-linux-gnu-x86_64.tar.gz"

# Download the tarball
echo "Downloading $TAR_URL..."
curl -L -o macchina.tar.gz "$TAR_URL"
echo "Extracting macchina.tar.gz..."
tar -xzf macchina.tar.gz

# Move files to /usr/local/bin
echo "Moving files to $INSTALL_DIR..."
sudo mv macchina $INSTALL_DIR

# check if macchina is installed
if ! command -v macchina &> /dev/null
then
    echo "macchina could not be found"
    exit
fi

# Cleanup
echo "Cleaning up"
rm macchina.tar.gz

