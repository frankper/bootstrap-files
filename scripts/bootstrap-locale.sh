#!/usr/bin/env bash
# declare locale   
declare -a UserLocales=(en_US.UTF-8 en_GB.UTF-8)
export TZ=Etc/UT
export LANG=C.UTF-8
export LC_ALL=C.UTF-8
export DEBIAN_FRONTEND=noninteractive
sudo apt-get install -qy tzdata locales

for loc in ${UserLocales[@]}; do 
    sudo locale-gen $loc
    sudo update-locale LANG=${loc}
    locale -a
done
