#!/usr/bin/env bash
# Substitute BIN for your bin directory.
# Substitute VERSION for the current released version.
BIN="/usr/local/bin" && \
VERSION="1.0.0" && \
sudo curl -sSL \
    "https://github.com/bufbuild/buf/releases/download/v${VERSION}/buf-$(uname -s)-$(uname -m)" \
    -o "${BIN}/buf" && \
  sudo chmod +x "${BIN}/buf"

echo -e "\e[1;31m BUF INSTALL DONE \e[0m"
