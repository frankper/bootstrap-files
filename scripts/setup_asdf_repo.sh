#!/usr/bin/env bash
unset  HashicorpPluginsToInstall PluginsToInstall SoftVersions
# vars
COMP_NAME=asdf
declare -a HashicorpPluginsToInstall=(boundary consul nomad packer sentinel serf terraform vault waypoint)
declare -a PluginsToInstall=(
    kubectl 
    tmux 
    awscli
    direnv
    yarn
    stern
    neovim
    bat
    eza
    opentofu
    "azure-cli https://github.com/itspngu/asdf-azure-cli"
    "asdf-alias https://github.com/andrewthauer/asdf-alias.git" 
    "argo https://github.com/sudermanjr/asdf-argo.git" 
    "rust https://github.com/asdf-community/asdf-rust.git" 
    "1password-cli https://github.com/NeoHsu/asdf-1password-cli.git" 
    "eksctl https://github.com/elementalvoid/asdf-eksctl.git"
    "kustomize https://github.com/Banno/asdf-kustomize.git"
    "k9s https://github.com/looztra/asdf-k9s"
    "docker-compose https://github.com/virtualstaticvoid/asdf-docker-compose.git"
    "tmux https://github.com/aphecetche/asdf-tmux.git"
    "krew https://github.com/nlamirault/asdf-krew.git"
    "helm https://github.com/Antiarchitect/asdf-helm.git"
    "kubetail https://github.com/janpieper/asdf-kubetail.git"
    "tflint https://github.com/skyzyx/asdf-tflint"
    "nodejs https://github.com/asdf-vm/asdf-nodejs.git"
    "pulumi https://github.com/canha/asdf-pulumi.git"
    "kind https://github.com/reegnz/asdf-kind.git"
    "golang https://github.com/kennyp/asdf-golang.git"
    "hugo https://github.com/beardix/asdf-hugo.git"
    "ruby https://github.com/asdf-vm/asdf-ruby.git"
    "gcloud https://github.com/jthegedus/asdf-gcloud"
    "lazydocker https://github.com/comdotlinux/asdf-lazydocker.git"
    "teleport-ent https://github.com/highb/asdf-teleport-ent.git"
    "helmfile https://github.com/feniix/asdf-helmfile.git"
    "cilium-hubble https://github.com/NitriKx/asdf-cilium-hubble.git"
    "terragrunt https://github.com/lotia/asdf-terragrunt"
    "cilium-cli https://github.com/carnei-ro/asdf-cilium-cli.git"
    "trivy https://github.com/zufardhiyaulhaq/asdf-trivy.git"
    "cilium-hubble https://github.com/NitriKx/asdf-cilium-hubble.git"
    "minikube https://github.com/alvarobp/asdf-minikube.git"
    "yq https://github.com/sudermanjr/asdf-yq.git"
    "delta https://github.com/andweeb/asdf-delta.git"
    "istioctl https://github.com/virtualstaticvoid/asdf-istioctl.git"
    "doctl https://github.com/maristgeek/asdf-doctl.git"
    "clusterctl https://github.com/pfnet-research/asdf-clusterctl.git"
)

# source $HOME/.zshrc
#source $HOME/.zshrc

# adsf 
export PATH="${ASDF_DATA_DIR:-$HOME/.asdf}/shims:$PATH"
fpath=(${ASDF_DATA_DIR:-$HOME/.asdf}/completions $fpath)
#autoload -Uz compinit && compinit

# Check if asdf is installed
if ! command -v asdf &> /dev/null
then
    echo "asdf could not be found"
    exit
fi

# asdf hashicorp plugins installs
for i in ${!HashicorpPluginsToInstall[@]}; do 
    asdf plugin add ${HashicorpPluginsToInstall[i]} https://github.com/asdf-community/asdf-hashicorp.git
done
# asdf plugins installs
for z in ${!PluginsToInstall[@]}; do 
    asdf plugin add ${PluginsToInstall[z]}
done

