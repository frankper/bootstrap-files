#!/usr/bin/env bash
# tested in debian 10 

sudo apt-get update -qqy
# install the requirements
sudo apt-get install -qqy \
        ca-certificates \
        apt-transport-https \
        gnupg
# Download the Google  repository GPG keys
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
# Crete the apt source file
echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
# Update the list of products
sudo apt-get update -qqy
# Install PowerShell
sudo apt-get install -qqy google-cloud-sdk

which gcloud
