#!/usr/bin/env bash 
## tested in ubuntu 22.04 , D12
declare packagelist1=(
    gitup
    pipenv
    virtualenv
    tldr
)

function InstallPipxAddons() {
for i in ${!packagelist1[@]}; do 
    pipx install ${packagelist1[i]}
done
}

## install pipx and pipx packages 
python3 -m pip install --user pipx
python3 -m pipx ensurepath
InstallPipxAddons