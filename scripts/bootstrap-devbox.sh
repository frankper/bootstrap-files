#!/usr/bin/env bash
# config for devbox global
mkdir -p $HOME/.local/share/devbox/global/default/
cp $HOME/utils/dotfiles/devbox/devbox.json $HOME/.local/share/devbox/global/default/
#cp $HOME/utils/dotfiles/devbox/devbox.lock $HOME/.local/share/devbox/global/default/
if [ -f $HOME/.bashrc ]; then
    echo 'eval "$(devbox global shellenv --init-hook)"' >> ~/.bashrc
else
    echo 'eval "$(devbox global shellenv --init-hook)"' >> ~/.zshrc
fi
devbox global install
eval "$(devbox global shellenv)"
#devbox completion bash > /etc/bash_completion.d/devbox
#devbox completion zsh > "${fpath[1]}/_devbox"
#eval "$(devbox global shellenv --recompute)"