#!/usr/bin/env bash
CURRENT_VERSION=$(curl -Ls https://api.github.com/repos/Versent/saml2aws/releases/latest | grep 'tag_name' | cut -d'v' -f2 | cut -d'"' -f1)
wget -c https://github.com/Versent/saml2aws/releases/download/v${CURRENT_VERSION}/saml2aws_${CURRENT_VERSION}_linux_amd64.tar.gz -O - | tar -xzv -C ~/
chmod u+x ~/saml2aws
sudo mv ~/saml2aws /usr/localbin
hash -r
saml2aws --version
echo -e "\e[1;31m SAML2AWS INSTALL DONE \e[0m"
