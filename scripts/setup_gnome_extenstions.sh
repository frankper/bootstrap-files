#!/usr/bin/env bash 
# tested in ubuntu 22.04,debian 12,arch
# generated by copilot

## install pipx and pipx packages 
python3 -m pip install --user pipx
python3 -m pipx ensurepath

# Check if a file path is provided
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <file-path>"
    exit 1
fi

FILE=$1

# Check if the file exists
if [ ! -f "$FILE" ]; then
    echo "File not found: $FILE"
    exit 1
fi

# check if pipx is installed

if ! command -v pipx &> /dev/null; then
    echo "pipx could not be found"
    exit
fi

## check if gext is installed via pip 
if ! command -v gext &> /dev/null; then
    echo "gext could not be found"
    echo "installing gext"
    pipx install gext
fi

# Read each line in the file and install the extension using gext
while IFS= read -r package; do
    echo "Installing $package..."
    gext install "$package"
done < "$FILE"

echo "All gnome extensions are installed."
