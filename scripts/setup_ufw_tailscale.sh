#!/usr/bin/env bash 
# tested in ubuntu 22.04,debian 12,arch

# Check if tailscale is installed
if ! command -v sudo tailscale &> /dev/null
then
    echo "tailscale could not be found"
    exit
fi

# Check if ufw is installed
if ! command -v sudo ufw &> /dev/null
then
    echo "ufw could not be found"
    exit
fi

lockServerTailscale() {
sudo ufw enable
sudo ufw allow in on tailscale0
sudo ufw default deny incoming
sudo ufw default allow outgoing
sudo ufw enable
sudo ufw reload
#sudo service ssh restart
}

lockServerTailscale