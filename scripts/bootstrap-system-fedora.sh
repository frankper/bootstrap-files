#!/usr/bin/env bash
# tested on F38
sudo dnf update -y && dnf autoremove -y
xargs sudo dnf install -y < $HOME/utils/scripts/system_packages_to_install_fedora.txt
sudo dnf autoremove -y && sudo dnf clean all
sudo chsh $USER -s $(which zsh)
