#!/usr/bin/env bash

# Set variables
REPO="Byron/dua-cli"
INSTALL_DIR="/usr/local/bin"

# Get latest release tag
LATEST_RELEASE=$(curl -s https://api.github.com/repos/${REPO}/releases/latest | grep -oP '"tag_name": "\K(.*?)(?=")')

# Construct download URL
TAR_URL="https://github.com/${REPO}/releases/download/${LATEST_RELEASE}/dua-${LATEST_RELEASE}-x86_64-unknown-linux-musl.tar.gz"

# Download the tarball
echo "Downloading $TAR_URL..."
curl -L -o dua.tar.gz "$TAR_URL"
echo "Extracting dua.tar.gz..."
tar -xzf dua.tar.gz

# Move files to /usr/local/bin
echo "Moving files to $INSTALL_DIR..."
sudo mv dua-${LATEST_RELEASE}-x86_64-unknown-linux-musl/dua $INSTALL_DIR

# check if dua is installed
if ! command -v dua &> /dev/null
then
    echo "dua could not be found"
    exit
fi

# Cleanup
echo "Cleaning up"
rm dua.tar.gz
