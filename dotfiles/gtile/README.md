# Gtile conf Tested with Gnome 45


# How to generate 

```
dconf dump /org/gnome/shell/extensions/gtile/ > gtile.conf
```

# How to load 

```
dconf load /org/gnome/shell/extensions/gtile/ < gtile.conf
```