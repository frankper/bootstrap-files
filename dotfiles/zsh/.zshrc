# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="aussiegeek"


# test comment 
# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"
HIST_STAMPS="dd.mm.yyyy"
HISTFILE="$HOME/dotfiles/zsh_history"
HISTSIZE=10000000000
SAVEHIST=10000000000

setopt BANG_HIST                 # Treat the '!' character specially during expansion.
setopt EXTENDED_HISTORY          # Write the history file in the ":start:elapsed;command" format.
setopt INC_APPEND_HISTORY        # Write to the history file immediately, not when the shell exits.
setopt SHARE_HISTORY             # Share history between all sessions.
setopt HIST_EXPIRE_DUPS_FIRST    # Expire duplicate entries first when trimming history.
setopt HIST_IGNORE_DUPS          # Don't record an entry that was just recorded again.
setopt HIST_IGNORE_ALL_DUPS      # Delete old recorded entry if new entry is a duplicate.
setopt HIST_FIND_NO_DUPS         # Do not display a line previously found.
setopt HIST_IGNORE_SPACE         # Don't record an entry starting with a space.
setopt HIST_SAVE_NO_DUPS         # Don't write duplicate entries in the history file.
setopt HIST_REDUCE_BLANKS        # Remove superfluous blanks before recording entry.
setopt HIST_VERIFY               # Don't execute immediately upon history expansion.
#setopt HIST_BEEP                 # Beep when accessing nonexistent history.

# GENERAL
# AUTOCOMPLETION
# initialize autocompletion
autoload -U compinit
compinit
# autocompletion using arrow keys (based on history)
bindkey '\e[A' history-search-backward
bindkey '\e[B' history-search-forward

# (bonus: Disable sound errors in Zsh)
# never beep
setopt NO_BEEP

# # new ones 
# 
# setopt INC_APPEND_HISTORY
# export HISTTIMEFORMAT="[%F %T] "
# setopt EXTENDED_HISTORY
# setopt HIST_FIND_NO_DUPS
# setopt HIST_IGNORE_ALL_DUPS

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  git
  kubectl
  history
  git-extras
  git-extra-commands # testing this out https://github.com/unixorn/git-extra-commands
  zsh-autosuggestions
  zsh-syntax-highlighting
  zsh-history-substring-search
  you-should-use
)

# auto update settings 
export DISABLE_AUTO_UPDATE="true"
source $ZSH/oh-my-zsh.sh

# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# decrease omz update verbosity
zstyle ':omz:update' mode disabled    
zstyle ':omz:update' verbosity minimal
zstyle ':completion:*' menu select

# source profiles main (change to os specific)
source $HOME/.zsh_profile_alias # contains all aliases and general config
source $HOME/.zsh_profile_env_linux # contains linux  specific conf
#source ~/.zsh_profile_apple # contains apple specific conf
#source ~/.zsh_profile_wsl2 # contains wsl2 specific conf

# source work files 
#source ~/.zsh_profiles_all/.zsh_profile_weave
#source ~/.zsh_profiles_all/.zsh_profile_bitso
#source ~/.zsh_profiles_all/.zsh_profile_iotics
#source ~/.zsh_profiles_all/.zsh_profile_etx
#source $HOME/dotfiles/zsh_profiles_all/.zsh_profile_work_dx # contains work Specific config

# source other profiles
#source ~/.zsh_profiles_all/.zsh_profile_gh_cli
#source ~/.zsh_profiles_all/.zsh_profile_add # random stuff
#source ~/.zsh_profiles_all/.zsh_profile[NOTUSED] # legacy profile that contains all above

# starship conf
export STARSHIP_CACHE="$HOME/starship/cache"
export STARSHIP_CONFIG="$HOME/starship.toml"
eval "$(starship init zsh)"

autoload -U compinit; compinit

# macchina load
macchina