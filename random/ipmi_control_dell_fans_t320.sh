#!/usr/bin/env bash
# script to bootstrap ubuntu
# tested in ubuntu 20.04
# Functions Used 
# Function to print in specified color
function colorprintf () {
    case $1 in
        "red") tput setaf 1;;
        "green") tput setaf 2;;
        "orange") tput setaf 3;;
        "blue") tput setaf 4;;
        "purple") tput setaf 5;;
        "cyan") tput setaf 6;;
        "gray" | "grey") tput setaf 8;;
        "white") tput setaf 7;;
    esac
    echo "$2";
    tput sgr0
}

# set vars
SERVERIPMI=192.168.30.10
SERVERUNSER=root
SERVERPASS=calvin
# enable manual mode 
ipmitool -I lanplus -H $SERVERIPMI -U $SERVERUNSER -P $SERVERPASS raw 0x30 0x30 0x01 0x00
# enable 10% fan mode 
ipmitool -I lanplus -H $SERVERIPMI -U $SERVERUNSER -P $SERVERPASS raw 0x30 0x30 0x02 0xff 0x05
# enable 20% fan mode 
#ipmitool -I lanplus -H $SERVERIPMI -U $SERVERUNSER -P $SERVERPASS raw 0x30 0x30 0x02 0xff 0x14
# enable 30% fan mode 
# ipmitool -I lanplus -H $SERVERIPMI -U $SERVERUNSER -P $SERVERPASS raw 0x30 0x30 0x02 0xff 0x1e
# enable 40% fan mode 
#ipmitool -I lanplus -H $SERVERIPMI -U $SERVERUNSER -P $SERVERPASS raw 0x30 0x30 0x02 0xff 0x28
# enable 100% fan mode 
#ipmitool -I lanplus -H $SERVERIPMI -U $SERVERUNSER -P $SERVERPASS raw 0x30 0x30 0x02 0xff 0x64
# enable 0% fan mode 
#ipmitool -I lanplus -H $SERVERIPMI -U $SERVERUNSER -P $SERVERPASS raw 0x30 0x30 0x02 0xff 0x00
# enable AUTO fan mode 
#ipmitool -I lanplus -H $SERVERIPMI -U $SERVERUNSER -P $SERVERPASS raw 0x30 0x30 0x01 0x01