Table of Contents
=================

* [Table of Contents](#table-of-contents)
* [Bootstrap files](#bootstrap-files)
* [Tools included](#tools-included)
   * [Programming languages](#programming-languages)
   * [Network utils](#network-utils)
   * [Shell utils](#shell-utils)
   * [Dev utils](#dev-utils)
   * [Kubernetes tools](#kubernetes-tools)
   * [Cloud tools](#cloud-tools)
   * [Custom dotfiles for :](#custom-dotfiles-for-)
   * [Package Managers and list of installed packages :](#package-managers-and-list-of-installed-packages-)
      * [Various Operations systems](#various-operations-systems)
      * [Asdf plugins/packages](#asdf-pluginspackages)
      * [Pipx Packages](#pipx-packages)
      * [Brew Packages (Although not being used)](#brew-packages-although-not-being-used)
      * [Arkade Packages (Although not being used)](#arkade-packages-although-not-being-used)
* [<strong>If you want to tweak this with your personal conf</strong>](#if-you-want-to-tweak-this-with-your-personal-conf)
* [Dot Files Configuration](#dot-files-configuration)
* [<strong>Links of some of the tools used/installed</strong>](#links-of-some-of-the-tools-usedinstalled)

<!-- Created by https://github.com/ekalinin/github-markdown-toc -->

# Bootstrap files

Bootstrap Scripts and dotfiles needed for my linux installations

This repo is being used by my [VagrantVM Repo](https://gitlab.com/frankper/mvg)

[Git-crypt](https://github.com/AGWA/git-crypt) has beed used to encrypt some of my secrets in the repo (under [private](https://gitlab.com/frankper/bootstrap-files/-/blob/main/private)) as stated in the [.gitattributes file](https://gitlab.com/frankper/bootstrap-files/-/blob/main/.gitattributes)

Quick and dirty use clone the repo
```shell
git clone https://gitlab.com/frankper/bootstrap-files.git utils
```
and start running scripts like 
```shell
./utils/scripts/install_arkade.sh
```


# Tools included

## Programming languages
* Go Rust Python ( ruby nodejs available via asdf) 
## Network utils 
* dnsutils arp-scan sipcalc bmon ipmitool traceroute nmap
## Shell utils
* sudo cryptsetup fzf bat wget git curl zsh vim neovim ncdu exa bat fd duf tldr ranger starship tmux direnv asdf
## Dev utils
* python-pip base-devel git github-cli gitlab-cli docker docker-compose
## Kubernetes tools
* k3s kind jq yq kubectl helm fluxctl kustomize skaffold telepresense arkade kubetail k9s eksctl clusterctl istioctl
## Cloud tools 
* azure-cli gcloud aws-cli sops eksctl terraform pulumi tflint
## Custom dotfiles for :
* neovim ohMyZsh tmux starship
## Package Managers and list of installed packages : 
### Various Operations systems
You will find the setup for each OS [in the scripts folder](https://gitlab.com/frankper/bootstrap-files/-/blob/main/scripts)
The boostrap script will use the respective `system_packages_to_install-*` list
For examples [ubuntu bootstrap script](https://gitlab.com/frankper/bootstrap-files/-/blob/main/scripts/bootstrap-system-ubuntu.sh) and the [ubuntu package list](https://gitlab.com/frankper/bootstrap-files/-/blob/main/scripts/setup_asdf_apps_dev.sh)
### Asdf plugins/packages
[List of repos](https://gitlab.com/frankper/bootstrap-files/-/blob/main/scripts/setup_asdf_repo.sh)
[List of packages installed](https://gitlab.com/frankper/bootstrap-files/-/blob/main/scripts/setup_asdf_apps_dev.sh)
### Pipx Packages
[List](https://gitlab.com/frankper/bootstrap-files/-/blob/main/scripts/system_packages_pipx.txt)
### Brew Packages (Although not being used)
[List](https://gitlab.com/frankper/bootstrap-files/-/blob/main/scripts/Brewfile)
### Arkade Packages (Although not being used)
[List](https://gitlab.com/frankper/bootstrap-files/-/blob/main/scripts/setup_arkade.sh)

# **If you want to tweak this with your personal conf**

* Modify [pipx packages](https://gitlab.com/frankper/bootstrap-files/-/blob/main/scripts/system_packages_pipx.txt)
* Modify [asdf plugins](https://gitlab.com/frankper/bootstrap-files/-/blob/main/scripts/setup_asdf_repo.sh) ,[asdf apps](https://gitlab.com/frankper/bootstrap-files/-/blob/main/scripts/setup_asdf_dev_apps.sh) or [global defaults](https://gitlab.com/frankper/bootstrap-files/-/blob/main/dotfiles/asdf/.tool-verions)
* Modify Brew packages [here](https://gitlab.com/frankper/bootstrap-files/-/blob/main/scripts/Brewfile)
* Modify Arkade packages [here](https://gitlab.com/frankper/bootstrap-files/-/blob/main/scripts/setup_arkade.sh)
* Modify Krew Plugins [here](https://gitlab.com/frankper/bootstrap-files/-/blob/main/scripts/setup_krew.sh)
* Zsh config is [here](https://gitlab.com/frankper/bootstrap-files/-/tree/main/dotfiles/zsh)
* Git config is [here](https://gitlab.com/frankper/bootstrap-files/-/tree/main/dotfiles/git) 
* Starship config is [here](https://gitlab.com/frankper/bootstrap-files/-/tree/main/dotfiles/starship)
* Tmux config is pulled from [here](https://github.com/frankperrakis/tmux-config)
* To modify the terminal werlcome message read [here](https://github.com/Macchina-CLI/macchina/wiki/Customization)
* To change aliases set in ZSH do it [here](https://gitlab.com/frankper/bootstrap-files/-/blob/main/dotfiles/zsh/.zsh_profile_alias)
* To add custom/manual plugins in OhMyZsh do it [here](https://gitlab.com/frankper/bootstrap-files/-/blob/main/scripts/setup_omz_plugins.sh)

# Dot Files Configuration
* ZSH history conf and work profiles are [here](https://gitlab.com/frankper/bootstrap-files/-/tree/main/private/zsh_profiles_all)

# **Links of some of the tools used/installed**
* [Scott Lowe's Learning Tools github repo](https://github.com/scottslowe/learning-tools)
* [Docker](https://docs.docker.com/) 
* [Spacevim](https://github.com/SpaceVim/SpaceVim)
* [Lazyvim](https://github.com/LazyVim/LazyVim)
* [Starship](https://starship.rs)
* [Arcade](https://github.com/alexellis/arkade)
* [Oh My Zsh](https://ohmyz.sh/)
* [Krew Plugin Manager](https://krew.sigs.k8s.io/)
* [Telepresense](https://www.telepresence.io/)
* [Pipx](https://pypa.github.io/pipx/)
* [Skaffold](https://skaffold.dev/)
* [Helm](https://helm.sh/)
* [Asdf](https://github.com/asdf-vm/asdf)
* [Direnv](https://direnv.net/)
* [Macchina](https://github.com/Macchina-CLI/macchina)
* [Git Quick Stats](https://github.com/arzzen/git-quick-stats)
* [Lazydocker](https://github.com/jesseduffield/lazydocker)
* [Tailscale](https://tailscale.com/)
* [K3s](https://k3s.io/)
* [Kind](kind.sigs.k8s.io/)
* [Git-crypt](https://github.com/AGWA/git-crypt)
* [Lazygit](https://github.com/jesseduffield/lazygit)